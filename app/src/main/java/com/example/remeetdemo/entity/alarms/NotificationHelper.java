package com.example.remeetdemo.entity.alarms;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.SystemClock;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

public class NotificationHelper {
    public static int ALARM_TYPE_RTC = 100;
    public static int REPEAT_TYPE_RTC = 100;
    public static String NOTIFICATION_TITLE = "notification title";
    public static String NOTIFICATION_TEXT = "notification text";
    public static String NEW_REMEET = "newRemeet";
    public static String NEW_REMEET_REPEAT = "newRemeetRepeat";
}
package com.example.remeetdemo.entity.alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;
import static com.example.remeetdemo.entity.alarms.NotificationHelper.ALARM_TYPE_RTC;
import static com.example.remeetdemo.entity.alarms.NotificationHelper.REPEAT_TYPE_RTC;

public class EventNotification {

    public void createNotification(Calendar notificationTime, Context context, String notificationTitle, String notificationMessage) {
        Intent myIntent = new Intent(context, RepeatableAsk.class);
        System.out.println(notificationTime.getTime().toString());
        myIntent.putExtra(NotificationHelper.NOTIFICATION_TITLE, notificationTitle);
        myIntent.putExtra(NotificationHelper.NOTIFICATION_TEXT, notificationMessage);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context
                , ALARM_TYPE_RTC++
                , myIntent
                , PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= 23) {
            // Wakes up the device in Doze Mode
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, notificationTime.getTimeInMillis(), // time in millis
                    pendingIntent);
        } else if (Build.VERSION.SDK_INT >= 19) {
            // Wakes up the device in Idle Mode
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, notificationTime.getTimeInMillis(), pendingIntent);
        } else {
            // Old APIs
            alarmManager.set(AlarmManager.RTC_WAKEUP, notificationTime.getTimeInMillis(), pendingIntent);
        }
    }

    public void createChecker(Context context) {
        Intent myIntent = new Intent(context, ChangeChecker.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context
                , REPEAT_TYPE_RTC
                , myIntent
                , PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), AlarmManager.INTERVAL_FIFTEEN_MINUTES / 15,
                pendingIntent);
    }

    public void cancelNOtifications(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
    }
}

package com.example.remeetdemo.entity.alarms;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.icu.util.Measure;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.example.remeetdemo.LoginActivity;
import com.example.remeetdemo.backend.Meetings;
import com.example.remeetdemo.entity.Event;
import com.example.remeetdemo.session.SessionManager;

import java.util.List;

public class ChangeChecker extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("@$@$@$@$@$@$@$@");
        List<Event> newMeetings = new Meetings().getMeetings(context, new SessionManager(context.getApplicationContext()).getToken());
        List<Event> oldMeetings = new Meetings().getUserMeetingsFromDB(context);
        int i = 0;
        for (i = 0; i < newMeetings.size(); i++) {
            if (checkMeeting(newMeetings.get(i), oldMeetings)) {
                createNotification(context, newMeetings.get(i));
            }
        }
        new Meetings().getUserMeetings(new SessionManager(context.getApplicationContext()).getToken(), context);
    }

    private boolean checkMeeting(Event event, List<Event> base) {
        for (int i = 0; i < base.size(); i++) {
            if (event.getId() == base.get(i).getId()) {
                return !event.comparator(base.get(i));
            }
        }
        return true;
    }

    private void createNotification(Context context, Event event) {
        Intent intentToRepeat = new Intent(context, LoginActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(context, NotificationHelper.REPEAT_TYPE_RTC, intentToRepeat, PendingIntent.FLAG_CANCEL_CURRENT);
        Notification repeatedNotification = buildLocalNotification(context
                , pendingIntent
                , "Встреча " + event.getName() + " изменилась"
                , "Смотри подробности в приложении")
                .build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            System.out.println("HHHHHHHHHH");
            CharSequence name = "Notification O";
            String description = "Jkfg ooo";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NotificationHelper.NEW_REMEET_REPEAT, name, importance);
            channel.setDescription(description);
            // Register the channel with    the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(NotificationHelper.REPEAT_TYPE_RTC, repeatedNotification);
    }

    private NotificationCompat.Builder buildLocalNotification(Context context, PendingIntent pendingIntent, String title, String message) {
        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(context, NotificationHelper.NEW_REMEET_REPEAT)
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(android.R.drawable.star_on)
//                        .setStyle(new NotificationCompat.InboxStyle())
                        .setVisibility(Notification.VISIBILITY_PUBLIC)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        long[] vibrate = new long[] { 1000, 1000, 1000, 1000, 1000 };
        builder.setVibrate(vibrate);
        return builder;
    }
}

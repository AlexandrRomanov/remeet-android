package com.example.remeetdemo.entity;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.remeetdemo.backend.Meetings;
import com.example.remeetdemo.database.MeetingsDB;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Event {

    private int id;

    private String timeBefore;
    private String name;
    private Date time;
    private String description;

    private boolean isMute;

    public Event(String name, String time, String description, boolean isMute, int id) {
        this.name = name;
        this.time = stringToDate(time);
        this.description = description;
        this.isMute = isMute;
        this.id = id;
        this.timeBefore = "15";
    }

    public Event(String name, Date time, String description, boolean isMute, int id, String timeBefore) {
        this.name = name;
        this.time = time;
        this.description = description;
        this.isMute = isMute;
        this.id = id;
        this.timeBefore = timeBefore;
    }

    public Event(@NonNull Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(MeetingsDB.ID));
        this.name = cursor.getString(cursor.getColumnIndex(MeetingsDB.NAME));
        this.time = cursorStringToDate(cursor.getString(cursor.getColumnIndex(MeetingsDB.TIME)));
        this.isMute = cursor.getInt(cursor.getColumnIndex(MeetingsDB.MUTE)) == 1;
        this.timeBefore = cursor.getString(cursor.getColumnIndex(MeetingsDB.TIME_BEFORE));
        this.description = cursor.getString(cursor.getColumnIndex(MeetingsDB.DESCRIPTION));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = stringToDate(time);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isMute() {
        return isMute;
    }

    public void setMute(boolean mute) {
        isMute = mute;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimeBefore() {
        return timeBefore;
    }

    public void setTimeBefore(String timeBefore) {
        this.timeBefore = timeBefore;
    }

    public String toString() {
        return name;
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MeetingsDB.ID, id);
        contentValues.put(MeetingsDB.NAME, name);
        contentValues.put(MeetingsDB.TIME, time.toString());
        contentValues.put(MeetingsDB.DESCRIPTION, description);
        contentValues.put(MeetingsDB.TIME_BEFORE, timeBefore);
        contentValues.put(MeetingsDB.MUTE, isMute);
        return contentValues;
    }

    @Nullable
    private Date stringToDate(String time) {
        try {
            return new SimpleDateFormat("y-MM-d'T'H:m:s.S z", Locale.ENGLISH).parse(time.substring(0, time.length() - 6) + " GMT" + time.substring(time.length() - 6));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    private Date cursorStringToDate(String time) {
        try {
            System.out.println(time);
            return new SimpleDateFormat("E MMM d H:m:s z y", Locale.ENGLISH).parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean comparator(Event event) {
        return name.equals(event.getName()) && time.equals(event.getTime()) && description.equals(event.getDescription());
    }
}

package com.example.remeetdemo.entity.alarms;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.example.remeetdemo.LoginActivity;
import com.example.remeetdemo.MainActivity;

public class RepeatableAsk extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Intent для вызова приложения по клику.
        //Мы хотим запустить наше приложение (главную активность) при клике на уведомлении
        System.out.println(intent);
        Intent intentToRepeat = new Intent(context, LoginActivity.class);
        //настроим флаг для перезапуска приложения
//        intentToRepeat.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(context, NotificationHelper.ALARM_TYPE_RTC, intentToRepeat, PendingIntent.FLAG_CANCEL_CURRENT);

        //Создаём уведомление
        Notification repeatedNotification = buildLocalNotification(context
                , pendingIntent
                , intent.getStringExtra(NotificationHelper.NOTIFICATION_TITLE)
                , intent.getStringExtra(NotificationHelper.NOTIFICATION_TEXT))
                .build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            System.out.println("HHHHHHHHHH");
            CharSequence name = "Notification O";
            String description = "Jkfg ooo";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NotificationHelper.NEW_REMEET, name, importance);
            channel.setDescription(description);
            // Register the channel with    the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(NotificationHelper.ALARM_TYPE_RTC++, repeatedNotification);
//        notificationManager.notify(null, NotificationHelper.ALARM_TYPE_RTC, repeatedNotification);

        //Отправляем уведомление
//        NotificationHelper.getNotificationManager(context).notify(NotificationHelper.ALARM_TYPE_RTC, repeatedNotification);
    }

    public NotificationCompat.Builder buildLocalNotification(Context context, PendingIntent pendingIntent, String title, String message) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChanels();
        }
        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(context, NotificationHelper.NEW_REMEET)
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(android.R.drawable.star_on)
//                        .setStyle(new NotificationCompat.InboxStyle())
                        .setVisibility(Notification.VISIBILITY_PUBLIC)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        long[] vibrate = new long[] { 1000, 1000, 1000, 1000, 1000 };
        builder.setVibrate(vibrate);
        return builder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createChanels() {
        NotificationChannel androidChannel = new NotificationChannel("remeet",
                "android_channel", NotificationManager.IMPORTANCE_HIGH);
        // Sets whether notifications posted to this channel should display notification lights
        androidChannel.enableLights(true);
        // Sets whether notification posted to this channel should vibrate.
        androidChannel.enableVibration(true);
        // Sets the notification light color for notifications posted to this channel
        androidChannel.setLightColor(Color.GREEN);
        // Sets whether notifications posted to this channel appear on the lockscreen or not
        androidChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library

    }
}
package com.example.remeetdemo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class MeetingsDB extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "remeet";

    public static final String TABLE_NAME = "meetingsWithTime";
    public static final String NAME = "name";
    public static final String TIME = "time";
    public static final String DESCRIPTION = "description";
    public static final String MUTE = "mute";
    public static final String ID = "id";
    public static final String TIME_BEFORE = "timeBefore";

    public static final String CREATE_TABLE = "create table " + TABLE_NAME
            + " (" + ID + " integer primary key, "
            + NAME + " TEXT, "
            + TIME + " TEXT, "
            + MUTE + " INTEGER, "
            + TIME_BEFORE + " TEXT, "
            + DESCRIPTION + " TEXT)";

    public MeetingsDB(Context context) {
        super(context, DB_NAME, null,DB_VERSION);
    }

    public MeetingsDB(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int j) {
    }
}

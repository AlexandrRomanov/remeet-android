package com.example.remeetdemo.backend;
import android.os.AsyncTask;

import com.example.remeetdemo.entity.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ManagedMeetings extends AsyncTask<String, Void, List<Event>> {

    @Override
    protected List<Event> doInBackground(String... strings) {
        List<Event> meetigsArray = new ArrayList<>();

        try {
            URL url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();

            String inputString;
            while ((inputString = bufferedReader.readLine()) != null) {
                builder.append(inputString);
            }

            JSONObject topLevel = new JSONObject(builder.toString());
            JSONArray managedMeetings = topLevel.getJSONArray("managed_meetings");

            for (int i = 0; i < managedMeetings.length(); i++) {
                JSONObject news = managedMeetings.getJSONObject(i);
                if (!news.getString("next_time_real").equals("null")) {
                    meetigsArray.add(new Event(news.getString("title")
                            , news.getString("next_time_real")
                            , news.getString("place_address")
                            , managedMeetings.getJSONObject(i).getBoolean("mute")
                            , news.getInt("id")));
                }
            }

            urlConnection.disconnect();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return meetigsArray;
    }

    @Override
    protected void onPostExecute(List<Event> s) {
        super.onPostExecute(s);
    }
}
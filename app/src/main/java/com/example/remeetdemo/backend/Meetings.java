package com.example.remeetdemo.backend;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.example.remeetdemo.database.MeetingsDB;
import com.example.remeetdemo.entity.Event;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Meetings {

    private String[] columns = {MeetingsDB.ID, MeetingsDB.NAME, MeetingsDB.TIME, MeetingsDB.MUTE, MeetingsDB.TIME_BEFORE, MeetingsDB.DESCRIPTION};

    public List<Event> getManagedMeetings(String apiKey) {
        Uri uri = Uri.parse("http://remeet.ru/meetings/api").buildUpon()
                .appendQueryParameter("auth_token", apiKey)
                .build();
        System.out.println(uri.toString());
        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        ManagedMeetings managedMeetings = new ManagedMeetings();

        List<Event> requestResult;
        try {
            assert url != null;
            requestResult = managedMeetings.execute(url.toString()).get();
            //Log.i(TAG, requestResult);
            return requestResult;

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Event> getUserMeetings(String apiKey, Context context) {
        if (!CheckNetwork.hasConnection(context)) {
            return null;
        }
        Uri uri = Uri.parse("http://remeet.ru/meetings/api").buildUpon()
                .appendQueryParameter("auth_token", apiKey)
                .build();
        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        UserMeetings userMeetings = new UserMeetings();

        List<Event> requestResult = null;
        try {
            assert url != null;
            requestResult = userMeetings.execute(url.toString()).get();
            //Log.i(TAG, requestResult);

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        MeetingsDB meetingsDB = new MeetingsDB(context);
        SQLiteDatabase database = meetingsDB.getWritableDatabase();
        for (int i = 0; i < requestResult.size(); i++) {
            database.insertWithOnConflict(MeetingsDB.TABLE_NAME, null, requestResult.get(i).toContentValues(), SQLiteDatabase.CONFLICT_REPLACE);
        }
        database.close();
        return requestResult;
    }

    public void changeMute(int id, Context context, String apiKey) {
        if (!CheckNetwork.hasConnection(context)) {
            return ;
        }
        Uri uri = Uri.parse("http://remeet.ru/meetings/remute." + id).buildUpon()
                .appendQueryParameter("auth_token", apiKey)
                .build();
        System.out.println(uri.toString());
        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        ChangeMute userMeetings = new ChangeMute();

        try {
            assert url != null;
            userMeetings.execute(url.toString()).get();
            //Log.i(TAG, requestResult);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        MeetingsDB meetingsDB = new MeetingsDB(context);
        SQLiteDatabase database = meetingsDB.getReadableDatabase();
        Cursor cursor = database.query(MeetingsDB.TABLE_NAME, columns,"id = ?", new String[]{Integer.toString(id)}, null, null, null);
        cursor.moveToNext();
        Event event = new Event(cursor);
        event.setMute(!event.isMute());
        database.update(MeetingsDB.TABLE_NAME, event.toContentValues(), "id = ?", new String[]{Integer.toString(id)});
        database.close();
    }

    public List<Event> getUserMeetingsFromDB(Context context) {
        List<Event> requestResult = new ArrayList<>();
        MeetingsDB meetingsDB = new MeetingsDB(context);
        SQLiteDatabase database = meetingsDB.getReadableDatabase();
        Cursor cursor = database.query(MeetingsDB.TABLE_NAME, columns,null, null, null, null, null);
        while (cursor.moveToNext())
        {
            requestResult.add(new Event(cursor));
        }
        database.close();
        return requestResult;
    }

    public void changeTimeBefore(int id, String timeBefore, Context context) {
        MeetingsDB meetingsDB = new MeetingsDB(context);
        SQLiteDatabase database = meetingsDB.getReadableDatabase();
        Cursor cursor = database.query(MeetingsDB.TABLE_NAME, columns,"id = ?", new String[]{Integer.toString(id)}, null, null, null);
        cursor.moveToNext();
        Event event = new Event(cursor);
        event.setTimeBefore(timeBefore);
        database.update(MeetingsDB.TABLE_NAME, event.toContentValues(), "id = ?", new String[]{Integer.toString(id)});
        database.close();
    }

    public void clearTable(Context context) {
        MeetingsDB meetingsDB = new MeetingsDB(context);
        SQLiteDatabase database = meetingsDB.getReadableDatabase();
        database.delete(MeetingsDB.TABLE_NAME, null, null);
        database.close();
    }

    public List<Event> getMeetings(Context context, String apiKey)
    {
        if (!CheckNetwork.hasConnection(context)) {
            return null;
        }
        Uri uri = Uri.parse("http://remeet.ru/meetings/api").buildUpon()
                .appendQueryParameter("auth_token", apiKey)
                .build();
        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        UserMeetings userMeetings = new UserMeetings();

        List<Event> requestResult = null;
        try {
            assert url != null;
            requestResult = userMeetings.execute(url.toString()).get();
            //Log.i(TAG, requestResult);

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return requestResult;
    }
}

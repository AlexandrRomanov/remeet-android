package com.example.remeetdemo.backend;

import android.net.Uri;

import com.example.remeetdemo.entity.Event;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class OtherInfo {

    public String getUserName(String apiKey) {
        Uri uri = Uri.parse("http://remeet.ru/meetings/api").buildUpon()
                .appendQueryParameter("auth_token", apiKey)
                .build();
        System.out.println(uri.toString());
        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        GetUserInfo userMeetings = new GetUserInfo();

        String requestResult;
        try {
            assert url != null;
            requestResult = userMeetings.execute(url.toString()).get();
            //Log.i(TAG, requestResult);
            return requestResult;

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }
}

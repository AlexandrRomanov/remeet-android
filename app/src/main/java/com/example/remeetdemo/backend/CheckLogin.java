package com.example.remeetdemo.backend;

import android.net.Uri;
import android.os.AsyncTask;

import com.example.remeetdemo.R;
import com.example.remeetdemo.entity.Event;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CheckLogin {
    public boolean isCorrectKey(String ApiKey) {
        Uri uri = Uri.parse("http://remeet.ru/meetings/api").buildUpon()
                .appendQueryParameter("auth_token", ApiKey)
                .build();
        System.out.println(uri.toString());
        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            assert url != null;
            return new UserLoginTask().execute(url.toString()).get();

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }

    public class UserLoginTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            URL url = null;
            try {
                url = new URL(params[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.getResponseCode();
                boolean ans = !urlConnection.toString().contains("http://remeet.ru/users/sign_in");
                urlConnection.disconnect();
                return ans;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            super.onPostExecute(success);
        }
    }
}

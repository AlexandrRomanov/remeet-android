package com.example.remeetdemo;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.remeetdemo.backend.Meetings;
import com.example.remeetdemo.entity.Event;
import com.example.remeetdemo.entity.alarms.EventNotification;
import com.example.remeetdemo.session.SessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private List<Event> events;
    private Context context;
    private boolean isWrap;

    private static int currentPosition = -1;

    DataAdapter(Context context, List<Event> events) {
//        System.out.println(events.get(0).getName());
        this.events = events;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Event event = events.get(position);
        holder.nameView.setText(event.getName());
        Date date = event.getTime();
        Calendar alarmTime = Calendar.getInstance();
        alarmTime.setTime(date);
        if (alarmTime.get(Calendar.DAY_OF_YEAR) > Calendar.getInstance().get(Calendar.DAY_OF_YEAR)) {
            holder.timeView.setText(new SimpleDateFormat("d.MM.Y").format(date));
        } else {
            holder.timeView.setText(new SimpleDateFormat("H:m").format(date));
        }
        holder.additionalInfo.setText(event.getDescription());
        holder.timeSpinner.setSelection(positionInList(event.getTimeBefore()));
        if (event.isMute()) {
            holder.mute.setEnabled(false);
        } else {
            holder.mute.setEnabled(true);
        }
        holder.item.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
        if(currentPosition == position) {
            if (holder.linearLayout.getVisibility() == View.VISIBLE) {
//                currentPosition = -1;
//                Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.collapse);
                if (isWrap) {
                    holder.linearLayout.setVisibility(View.GONE);
                }
//                holder.linearLayout.startAnimation(slideDown);
            } else {
//                Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.expand);
                holder.linearLayout.setVisibility(View.VISIBLE);
//                holder.linearLayout.startAnimation(slideDown);
            }
        } else {
            holder.linearLayout.setVisibility(View.GONE);
        }

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //getting the positionInList of the item to expand it
                currentPosition = position;
                isWrap = true;
                //reloding the list
                notifyDataSetChanged();
            }
        });

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //getting the positionInList of the item to expand it
                currentPosition = position;
                isWrap = false;

                //reloding the list
//                notifyDataSetChanged();
            }
        });

        holder.mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                event.setMute(true);
                new Meetings().changeMute(event.getId(), context, new SessionManager(context).getToken());

                notifyDataSetChanged();
            }
        });
        final boolean[] finalIsReady = new boolean[1];
        finalIsReady[0] = false;
        holder.timeSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                finalIsReady[0] = true;
                return false;
            }
        });
        holder.timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                if (finalIsReady[0]) {
                    finalIsReady[0] = false;
                    String[] choose = context.getResources().getStringArray(R.array.time_list);
                    if (event.isMute()) {
                        event.setMute(false);
                        new Meetings().changeMute(event.getId(), context, new SessionManager(context).getToken());
                    }
                    new Meetings().changeTimeBefore(event.getId(), choose[selectedItemPosition], context);
                    event.setTimeBefore(choose[selectedItemPosition]);
                    Calendar alarmTime = Calendar.getInstance();
                    alarmTime.setTime(event.getTime());
                    alarmTime.add(Calendar.MINUTE, -Integer.parseInt(event.getTimeBefore()));
                    new EventNotification().createNotification(alarmTime
                            , context
                            , "Скоро начнется встреча"
                            , "До встречи " + event.getName() + " осталось " + event.getTimeBefore());
                    notifyDataSetChanged();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        holder.onSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://remeet.ru/meetings/" + event.getId() + "?auth_token=" + new SessionManager(context).getToken();
                Uri uri = Uri.parse(url);
                Intent i= new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    private int positionInList(String time) {
        String[] choose = context.getResources().getStringArray(R.array.time_list);
        for (int i = 0; i < choose.length; i++) {
            if (choose[i].equals(time)) {
                return i;
            }
        }
        return 1000;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final TextView nameView, timeView, additionalInfo;

        final LinearLayout linearLayout;
        final RelativeLayout item;

        final Button mute;
        final Button onSite;

        final Spinner timeSpinner;

        ViewHolder(View view){
            super(view);
            nameView = (TextView) view.findViewById(R.id.name);
            timeView = (TextView) view.findViewById(R.id.time);
            additionalInfo = (TextView) view.findViewById(R.id.additionalInfo);

            linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
            item = (RelativeLayout) view.findViewById(R.id.item);

            timeSpinner = (Spinner) view.findViewById(R.id.spinner);
            mute = (Button) view.findViewById(R.id.mute);
            onSite = (Button) view.findViewById(R.id.on_site);
        }
    }
}
package com.example.remeetdemo;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.remeetdemo.backend.Meetings;
import com.example.remeetdemo.backend.OtherInfo;
import com.example.remeetdemo.entity.Event;
import com.example.remeetdemo.entity.alarms.EventNotification;
import com.example.remeetdemo.entity.alarms.RepeatableAsk;
import com.example.remeetdemo.session.SessionManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ScrollingActivity extends AppCompatActivity {

    private List<Event> meetings = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String name = new OtherInfo().getUserName(new SessionManager(getApplicationContext()).getToken());
        setTitle(name);
//        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_scrolling);
        setContentView(R.layout.activity_scrolling);
//        ActionBar actionbar = getActionBar();
//
//        TextView textview = new TextView(ScrollingActivity.this);
//        RelativeLayout.LayoutParams layoutparams = new RelativeLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
//        textview.setLayoutParams(layoutparams);
//        textview.setText("ActionBar Title");
//        textview.setTextColor(Color.MAGENTA);
//        textview.setGravity(Gravity.CENTER);
//        textview.setTextSize(30);
////        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        actionbar.setCustomView(textview);
        setData();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvToDoList);
        // создаем адаптер
        DataAdapter adapter = new DataAdapter(this, meetings);
        // устанавливаем для списка адаптер
        recyclerView.setAdapter(adapter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.SECOND, 10);
                new EventNotification().createNotification(calendar
                        , ScrollingActivity.this
                        , "Hi all"
                        , "I know what to do");
            }
        });
        new EventNotification().createChecker(ScrollingActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            new SessionManager(getApplicationContext()).setLogin(false);
            new Meetings().clearTable(getApplicationContext());
            Intent intent = new Intent(ScrollingActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setData() {
        meetings = new Meetings().getUserMeetingsFromDB(ScrollingActivity.this);
//        ArrayList<String> sportList = new ArrayList<>(Arrays.asList(data));
//        for (int i = 0; i < sportList.size(); i++) {
//            meetings.add(new Event(sportList.get(i), ""));
//        }
    }
}
